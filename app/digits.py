import numpy as np
from matplotlib import pyplot as plt
import cv2
from windows import setup_window

img = cv2.imread('../dataset/img_0516.jpg', cv2.IMREAD_GRAYSCALE)

setup_window('gray')
cv2.imshow('gray', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
