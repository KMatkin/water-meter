import cv2

w_width = 800
w_height = 600

def setup_window(name, window_width=w_width, window_height=w_height):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, w_width, w_height)
